import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TestTable } from './testtable/testtable.entity';
import { TestTableModule } from './testtable/testtable.module';

@Module({
  imports: [
    //TypeOrmModule.forRoot(),
    //TypeOrmModule.forFeature([TestTable]),
    TestTableModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
