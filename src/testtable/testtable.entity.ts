import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('testtable')
export class TestTable {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  key: string;

  @Column()
  value: string;
}
