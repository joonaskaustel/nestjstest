import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TestTableController } from './testtable.controller';
import { TestTableService } from './testtable.service';
import { TestTable } from './testtable.entity';

@Module({
  imports: [TypeOrmModule.forRoot(), TypeOrmModule.forFeature([TestTable])],
  controllers: [TestTableController],
  providers: [TestTableService],
  exports: [TestTableService],
})
export class TestTableModule {}
