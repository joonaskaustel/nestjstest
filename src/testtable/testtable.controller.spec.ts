import { Test, TestingModule } from '@nestjs/testing';
import { TestTableController } from './testtable.controller';
import { TestTableService } from './testtable.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { TestTable } from './testtable.entity';
import { ITestTable } from './testtable.interface';
import { TestTableRepository } from './testtable.repository';
import { Repository } from 'typeorm';

describe('TestTableController', () => {
  let testTableController: TestTableController;
  let testTableService: TestTableService;

  const mockRepository = {
    data: [{ id: 1, key: 'abc', value: '123' }],
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      exports: [
        {
          provide: 'TestTableRepository',
          useClass: Repository,
        },
      ],
      controllers: [TestTableController],
      providers: [
        TestTableService,
        {
          provide: getRepositoryToken(TestTable),
          useValue: mockRepository,
        },
      ],
    }).compile();

    testTableService = module.get<TestTableService>(TestTableService);
    testTableController = module.get<TestTableController>(TestTableController);
  });

  describe('getall', () => {
    it('should return an array of items in testtable table', async () => {
      const result = [
        {
          id: 1,
          key: 'abc',
          value: '123',
        },
      ];

      jest
        .spyOn(testTableService, 'getAll')
        .mockImplementation(async () => [
          { id: 123, key: 'asdasd', value: 'asdasd' },
        ]);

      expect(await testTableController.getAll()).toBe(result);
    });
  });
});
