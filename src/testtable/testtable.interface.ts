export interface ITestTable {
  id: number;
  key: string;
  value: string;
}
