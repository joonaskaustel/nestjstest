import { EntityRepository, Repository } from 'typeorm';
import { TestTable } from './testtable.entity';

@EntityRepository(TestTable)
export class TestTableRepository extends Repository<TestTable> {}
