import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TestTable } from './testtable.entity';
import { ITestTable } from './testtable.interface';

@Injectable()
export class TestTableService {
  constructor(
    @InjectRepository(TestTable)
    private readonly testtableRepository: Repository<TestTable>,
  ) {}

  async getAll(): Promise<ITestTable[]> {
    try {
      //return [];
      //return 'tesfdsat';
      return await this.testtableRepository.find();
      //const items = await this.testtable
      //  .createQueryBuilder('testtable')
      //  //.where('testtable.id = :id', { id: 2 })
      //  .getMany();
      //return items;
    } catch (err) {
      return err;
    }
  }
}
