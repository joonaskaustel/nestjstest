import { Controller, Get } from '@nestjs/common';
import { TestTableService } from './testtable.service';
import { ITestTable } from './testtable.interface';

@Controller('testtable')
export class TestTableController {
  constructor(private readonly testTableService: TestTableService) {}

  @Get()
  getAll(): Promise<ITestTable[]> {
    return this.testTableService.getAll();
  }
}
