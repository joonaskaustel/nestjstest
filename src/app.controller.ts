import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ITestTable } from './testtable/testtable.interface';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData(): string {
    return 'Hello World!';
  }
}
